//Shant Keshishian
//CSE 002
//Lab02
//2/1/19

//This program will keep track of and print the number of minutes, counts, and distance for each trip
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
      int secsTrip1=480;  // Variable for amount of seconds in trip #1
      int secsTrip2=3220;  // Variable for amount of seconds in trip #2
      int countsTrip1=1561;  // Variable for counts in trip #1
      int countsTrip2=9037; // Variable for counts in trip #2
      double wheelDiameter=27.0,  //Define diameter of bike wheel
  	  PI=3.14159, // Defining value of pi
  	  feetPerMile=5280, //Conversion for feet to miles
  	  inchesPerFoot=12,   //Conversion for inches to feet
  	  secondsPerMinute=60;  //Conversion for seconds to minutes
	    double distanceTrip1, distanceTrip2, totalDistance;  // Declare these variables as doubles
      
      System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+ " counts."); 
      // Convert seconds variable to minutes and print both minutes and count for trip#1
	    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+ " counts."); 
      // Convert seconds variable to minutes and print both minutes and count for trip#2

      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	// For each count, a rotation of the wheel travels the diameter in inches times PI
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //finds value for trip and converts units to miles in one step
	    totalDistance=distanceTrip1+distanceTrip2; //sum of both trips

      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class



