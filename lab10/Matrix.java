//Shant Keshishian
//CSE 002
//5/3/19

import java.util.Arrays;
import java.util.Random;

public class Matrix{
  
  public static int[][] increasingMatrix(int width, int height, boolean format){
    int[][] array = new int[height][width]; // Java's shortcut generating regular array of arrays for you
    int count = 1;
    if (format) { // generate row-major matrix
      for (int i = 0; i < height; i++) {
        for (int j = 0; j < width; j++) {
          array[i][j] = count;
          count++;
        }
      }
    } 
    else { // generate column-major matrix
      for (int j = 0; j < width; j++){
        for (int i = 0; i < height; i++) {
          array[i][j] = count;
          count++;
        }
      }
    }
    return array;
}
  
  public static void printMatrix(int[][] array, boolean format){
    if(format){ //check if boolean is true
      for(int i = 0; i < array.length; i++){
        for(int j = 0; j < array[i].length; j++){
          System.out.printf("%5s",array[i][j]); //prints array members in row major
        }
        System.out.println();
      }
      System.out.println();
      System.out.println();
    }
    else{ //if not true
      for(int j = 0; j < array.length; j++){
        for(int i = 0; i < array[j].length; i++){
          System.out.printf("%5s",array[j][i]); //prints array members in colum
        }
        System.out.println();
      }
      System.out.println();
      System.out.println();
    } 
  }
  
  public static int[][] translate(int[][] array){ //use this algorithm to convert from column major to row major
    int count = 1;
    for (int i = 0; i < array.length; i++) { 
        for (int j = 0; j < array[i].length; j++) {
          array[i][j] = count;
          count++;
        }
      }
    return array;
  }
  
  public static int[][] addMatrix(int[][] a, boolean formata, int[][] b, boolean formatb){
    if(a.length == b.length && a[0].length == b[0].length){ //check if matrixes are same length and width
      int[][] array = new int[a.length][a[0].length];
      for(int i = 0; i < a.length; i++){
        for(int j = 0; j < a[0].length; j++){
          array[i][j]= a[i][j] + b[i][j]; //add the array members
      }
    }
      return array;
  }
    else{ //if not same size, print this and return empty array
      System.out.println("Cannot add these matrixes");
      int[][] bad = new int[1][1];
      return bad;
    }
  }
    
  
  public static void main(String [] args){
    Random randomGenerator = new Random();
    int width1 = randomGenerator.nextInt(7)+1;
    int width2 = randomGenerator.nextInt(7)+1;
    int height1 = randomGenerator.nextInt(7)+1;
    int height2 = randomGenerator.nextInt(7)+1; //generate random numbers for dimensions
    
    int[][] array1 = increasingMatrix(width1, height1, true);
    int[][] array2 = increasingMatrix(width1, height1, false);
    int[][] array3 = increasingMatrix(width2, height2, true); //create the 3 matrixes
    
    printMatrix(array1, true);
    printMatrix(array2, false); //print the first two matrixes
    
    int [][] translate = translate(array2); //translate the column major matrix
    
    printMatrix(translate, true);
    
    int [][] add1 = addMatrix(array1, true, array2, false); //add array 1 and 2
    printMatrix(add1, true);
    int [][] add2 = addMatrix(array1, true, array3, true); //add array 1 and 3
    
  }
}