//Shant Keshishian
//CSE 002
//HW 07
//3/26/19

//This program will find the area of three different types of shapes using 4 methods
import java.util.Scanner; //Import scanner to enable user input

public class Area{ // Class required for every Java program
  public static Scanner myScanner = new Scanner(System.in); //Create static scanner for input method
    public static double input(){ //start of input method that checks if scanner values are valid
      while(true){ //creates infinite loop
        if(myScanner.hasNextDouble()){
          double val = myScanner.nextDouble(); //assign val to scanner input
          if(val < 0){
            System.out.println("ERROR!");
            System.out.print("Try Again: "); //if val is negative, reject value and prompt user again
          }
          else{
            return val; //loop can only be broken in this condition and val is returned
          } 
        }
        else{
          System.out.println("ERROR!: ");
          System.out.print("Try Again: ");
          String junkWord = myScanner.next(); //if scanner is not a double, reject string, store in junk variable and prompt user again
        } 
      }  
    }
    
    public static double circle(){ //method for area of a circle
      System.out.print("Input Radius: ");
      double radius = input(); //assign radius to user inputed value after it has gone through the input method
      return (Math.PI * radius * radius); //calculate area 
    }
    
    public static double triangle(){ //method for area of a triangle
      System.out.print("Input Base: ");
      double base = input(); //assign base to user inputed value after it has gone through the input method
      System.out.print("Input Height: ");
      double height = input(); //assign height to user inputed value after it has gone through the input method
      return (0.5 * base * height); //calculate area 
    }
      
    public static double rectangle(){ //method for area of a rectangle
      System.out.print("Input Length: ");
      double length = input(); //assign length to user inputed value after it has gone through the input method
      System.out.print("Input Width: ");
      double width = input(); //assign width to user inputed value after it has gone through the input method
      return (length * width); //calculate area 
    }

    public static void main(String[] args) { //Main method required for every Java program
      Scanner myScanner2 = new Scanner(System.in); //Create scanner
      while(true){ //creates infinite loop 
          System.out.println("Please enter 'circle', 'triangle', or 'rectangle' ");
        if(myScanner2.hasNext("circle")){
          double x = circle(); //assign x to returned value of circle()
          System.out.println("Area of the Circle is " + x);
          break; //exit infinite loop
        }
        else if(myScanner2.hasNext("triangle")){
          double y = triangle(); //assign y to returned value of circle()
          System.out.println("Area of the Triangle is " + y);
          break; //exit infinite loop
        }
        else if(myScanner2.hasNext("rectangle")){
          double z = rectangle(); //assign z to returned value of circle()
          System.out.println("Area of the Rectangle is " + z);
          break; //exit infinite loop
        }
        else{
          System.out.println("ERROR!");
          System.out.println("Not an acceptable shape!");
          String junk = myScanner2.nextLine(); //store string in junk variable and prompt user again
        }
      }
      
 }
}