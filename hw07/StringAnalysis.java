//Shant Keshishian
//CSE 002
//HW 07
//3/26/19

//This program will find the area of three different types of shapes using 4 methods
import java.util.Scanner; //Import scanner to enable user input

public class StringAnalysis{ // Class required for every Java program
  
    public static boolean Char(String inputString){ //method for analyzing entire string
      boolean bool = false;
      char val = 'a';
      int inp = inputString.length() - 1; //set variable equal to string character count
      int count = 0;
      for(int j = 1; j <= inp; j ++){ //create for loop that loops until j is equal to string character count
        val = inputString.charAt(j); //set val equal to current character in string
       if(val >= 'a' && val <= 'z'){
          count += 1; //if the character is a letter, increment counter
        }
      }
      if(count == inp){
        bool = true; //if the counter is equal to string character count, this means every character is a letter so the boolean should be true
      }
      return bool; //return bool value
    }
  
    public static boolean Char(String inputString, int numChar){ //method for analyzing part of string
      boolean bool = false;
      char val = 'a'; 
      int count = 0;
      for(int i = 1; i <= (numChar - 1); i ++){
        val = inputString.charAt(i); //set val equal to current character in string
        if(val >= 'a' && val <= 'z'){
          count += 1;  //if the character is a letter, increment counter
        }
      }
      if(count == (numChar - 1)){
        bool = true; //if the counter is equal to character count given by user, this means every character is a letter so the boolean should be true
      }
      return bool;
    }
  
    public static void main(String[] args) { //Main method required for every Java program
      Scanner myScanner = new Scanner(System.in); //create scanner for user input
      System.out.print("Please Enter a String for Analysis: ");
      String inputString = myScanner.next(); //prompt user for string and store string in variable
      System.out.print("Would like to analyze all characters? (yes or no): "); //prompt user if they would like to analyze all characters
      int numChar = 0;
      int val = 0;
      boolean x;
      boolean y;
      String junkWord = "";
      int num = 0;
      while(true){ //create infinite loop
        if(myScanner.hasNext("yes")){
          x = Char(inputString);
          System.out.println(x); //if user types yes, go to Char method that does not require integer input and print boolean
          break; //break out of infinite loop
        }
        else if(myScanner.hasNext("no")){
          while(true){ //loop through until conditions are met
            System.out.print("How many characters would you like to analyze?: ");
            if(myScanner.hasNextInt()){
              val = myScanner.nextInt(); //set variable to scanner input if the input is an integer
              if(val < 0){
                System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
              }
              else{
                numChar = val;
                break; //if conditions are met, set val1 to length and break out of infinite loop
              }
            }
            else{
              System.out.println("ERROR!: ");
              junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
            }
          }
          y = Char(inputString, numChar);
          System.out.println(y); //if user types yes, go to Char method that requires integer input and print boolean
          break; //break out of infinite loop
        }
        else{
          System.out.println("ERROR!");
          System.out.print("Input 'yes' or 'no': ");
          junkWord = myScanner.next(); //print error if yes or no are not typed and store string in junk variable
        }
      } 
    }
}