HW5 Rubric

Total Score: 95

  75/75: Program compiles

  20/25: At least 5 of these are correct:
    course number: good
    department name: you shouldn't really check if it's a 
        string by making sure it's not a int or double
        but i guess it's ok for now
    the number of times it meets in a week: good
    the time the class starts: you should limit the times like
        9999 would not make sense
    the instructor name: see above for string inputs
    and the number of students: good
