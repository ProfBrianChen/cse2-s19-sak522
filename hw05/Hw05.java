//Shant Keshishian
//CSE 002
//HW 05
//3/5/19

import java.util.Scanner; //add scanner to enable user input

// This program will let user know if the input is of the incorrect type 
public class Hw05{
    public static void main(String[] args) { // Main method required for every Java program

        Scanner myScanner = new Scanner(System.in); //Create your own Scanner using the first line of code to import a Scanner

        System.out.print("Enter Course Number (No Spaces): "); //print "Enter Course Number:"
        int courseNum = 0; //Declare variable for number of courses

        while(!myScanner.hasNextInt()){
            String junk1 = myScanner.nextLine();
            System.out.println("ERROR!");
            System.out.print("Enter Course Number (Integer Type): "); //while input is not an integer, print error and prompt user again
        }
        courseNum = myScanner.nextInt(); //assign scanner variable to courseNum


        System.out.print("Enter Department Name (No Spaces): "); //print "Enter Department Number:"
        String depName = "0"; //Declare variable depName

        while(myScanner.hasNextDouble() || myScanner.hasNextInt()){
            double junk2 = myScanner.nextDouble();
            System.out.println("ERROR!");
            System.out.print("Enter Department Name (String Type): "); //while input is a string, print error and prompt user again
        }
        depName = myScanner.next(); //assign scanner value to depName


        System.out.print("Enter Number of Classes Per Week (No Spaces): "); //print this statement
        int numClasses = 0; //Declare variable numClasses

        while(!myScanner.hasNextInt()){
            String junk3 = myScanner.nextLine();
            System.out.println("ERROR!");
            System.out.print("Enter Number of Classes Per Week (Integer Type): "); //while input is not an integer, print error and prompt user again
        }
        numClasses = myScanner.nextInt(); //assign scanner value to numClasses


        System.out.print("Enter Time Class Starts in XXXX form: "); //print this statement
        int time = 0; //Declare variable time

        while(!myScanner.hasNextInt()){
            String junk4 = myScanner.nextLine();
            System.out.println("ERROR!");
            System.out.print("Enter Time Class Starts (Integer Type): "); //while input is not an integer, print error and prompt user again
        }
        time = myScanner.nextInt(); //assign scanner value to time


        System.out.print("Enter Instructor Name (No Spaces): "); //print this statement
        String instructor = "0"; //Declare variable instructor

        while(myScanner.hasNextDouble() || myScanner.hasNextInt()){
            double junk5 = myScanner.nextDouble();
            System.out.println("ERROR!");
            System.out.print("Enter Instructor Name (String Type): "); //while input is a string, print error and prompt user again
        }
        instructor = myScanner.next(); //assign scanner value to instructor


        System.out.print("Enter Number of Students (No Spaces): "); //print this statement
        int students = 0; //Declare variable students

        while(!myScanner.hasNextInt()){
            String junk6 = myScanner.nextLine();
            System.out.println("ERROR!");
            System.out.print("Enter Number of Students (Integer Type): "); //while input is not an integer, print error and prompt user again
        }
        students = myScanner.nextInt(); //assign scanner value to students

        System.out.println("Course Number: " + courseNum);
        System.out.println("Department Name: " + depName);
        System.out.println("Number of Classes Per Week: " + numClasses);
        System.out.println("Time Class Starts: " + time);
        System.out.println("Instructor Name: " + instructor);
        System.out.println("Number of Students: " + students); //Print all of the saved variables

    }
}         
