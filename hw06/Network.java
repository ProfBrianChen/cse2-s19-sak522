//Shant Keshishian
//CSE 002
//HW 06
//3/19/19

import java.util.Scanner; //add scanner to enable user input

// This program will print a network of boxes after receiving user input
public class Network{
    public static void main(String[] args) { // Main method required for every Java program
    
    Scanner myScanner = new Scanner(System.in); //Create your own Scanner using the first line of code to import a Scanner
    int val1 = 0;
    int length = 0;
    String junkWord = ""; //Initialize variable to store incorrect string inputs
    while(true){ //loop through until conditions are met
       System.out.print("Enter Length: "); //Prompts user to enter integer
      if(myScanner.hasNextInt()){
        val1 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
        if(val1 < 0){
          System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
        }
        else{
          length = val1; 
          break; //if conditions are met, set val1 to length and break out of infinite loop
        }
      }
      else{
        System.out.println("ERROR!: ");
        junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
      }
     }
      
     int val2 = 0;
     int width = 0;
     while(true){ //loop through until conditions are met
        System.out.print("Enter Width: "); //Prompts user to enter integer
        if(myScanner.hasNextInt()){
          val2 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
            if(val2 < 0){
              System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
            }
            else{
              width = val2;
              break; //if conditions are met, set val2 to width and break out of infinite loop
            }
        }
        else{
          System.out.println("ERROR!: ");
          junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
        }
      }
      
     int val3 = 0;
     int size = 0;
     while(true){ //loop through until conditions are met
        System.out.print("Enter Square Size: "); //Prompts user to enter integer
        if(myScanner.hasNextInt()){
          val3 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
            if(val3 < 0){
              System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
            }
            else{
              size = val3;
              break; //if conditions are met, set val3 to size and break out of infinite loop
            }
        }
        else{
          System.out.println("ERROR!: ");
          junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
        }
      }
      
      int val4 = 0;
      int edgeSize = 0;
      while(true){ //loop through until conditions are met
        System.out.print("Enter Edge Size: "); //Prompts user to enter integer
        if(myScanner.hasNextInt()){
          val4 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
          if(val4 < 0){
            System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
          }
          else{
            edgeSize = val4;
            break; //if conditions are met, set val4 to edgeSize and break out of infinite loop
          }
        }
        else{
          System.out.println("ERROR!: ");
          junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
        }
      }
      
      int c = 0; 
      int y = 0;
      int v = 0;
      int f = 0; //initialize all remainder counting variables
      
      int x = 0;
      int r = 0;
      int w = 0; //initialize counter variables that must keep their values after each row is completed

      for(int z = 0; z < length; z++){ //loop that increments z after each row is completed
        int b = 0;
        int k = 0;
        int j = 1;
        int p = 0; //initialize counter variables that must be reset after each row is completed
        y = x % (size + edgeSize); // assign variable to be remainder of pattern
        
        if(y == 0 || y == size - 1){ //condition for the top and bottom of an individual box 
          for(int a = 0; a < width; a++){ //loop that cycles for each point in this section and determines what to print
            c = b % (size + edgeSize); // assign variable to be remainder of pattern
            if(size < edgeSize){ //condition where the dimensions of the box are smaller than the size of the edges of the "network" 
               if( c == 0 || c == size - 1){
                 System.out.print("#"); //ends of line should print #
               }
               else if(c <= size - 1){
                 System.out.print("-"); //middle of line should print -
               }
               else if(c >= size){ // when in between boxes 
                 if(size % 2 == 0){ // check if box dimension is even
                   if(y == size/2 || y == ((size/2) - 1)){
                     System.out.print("-"); //if y is equal to either of the two middle points if the box, print -
                   }
                   else{
                     System.out.print(" "); //if not print a space
                   }
                 }
                 else{ 
                   if(y == size/2){
                     System.out.print("-"); //if box dimensions are odd and y is the middle of the box, print -
                   }
                   else{
                     System.out.print(" "); //if not print a space
                   }
                 }
               }
              
            }
            else if(c == 0 || c == size - 1){ 
              System.out.print("#"); //if variable is equivalent to end of box and dimensions of box are larger than egde size, print #
            }
            else if(c <= size - 1){
              System.out.print("-"); //if variable in middle of ends, print -
            }
            else if(c >= size){
              System.out.print(" "); //if variable in between boxes, print a space
            }
            b++; //increment counter that changes the remainder
          }
          System.out.println("");
        }

        else if(y <= size - 1){ //condition for section in the middle of the box
          for(int q = 0; q < width; q++){ //loop that cycles for each point in this section and determines what to print
            v = k % (size + edgeSize); // assign variable to be remainder of pattern
            if( v == 0 || v == size - 1){
              System.out.print("|"); //if variable equivalent to ends of box, print |
            }
            else if(v <= size - 1){
              System.out.print(" "); //if variable in between ends of box, print a space
            }
            else if(v >= size){ //if variable corresponds to gap between boxes
              if(size % 2 == 0){ //check if dimensions of box are even
                if(y == size/2 || y == ((size/2) - 1)){
                  System.out.print("-"); //if y is equal to either of the two middle points if the box, print -
                }
                else{
                  System.out.print(" "); //if not print a space
                }
              }
              else{
                if(y == size/2){
                  System.out.print("-"); //if box dimensions are odd and y is the middle of the box, print -
                }
                else{
                  System.out.print(" "); //if not print a space
                }
              }
            }
            k++; //increment counter that changes remainder
          }
          System.out.println("");
        }

        else if(y >= size){ //condition for gap in between the boxes
          for(int t = 0; t < width; t++){ //loop that cycles for each point in this section and determines what to print
            f = j % (size + edgeSize); // assign variable to be remainder of pattern
            if(size % 2 == 0){ //check if dimensions of box are even
              if(f == size/2 || f == ((size/2) + 1)){
                System.out.print("|");  //if f is equal to either of the two middle points if the box, print |
              }
              else{
                System.out.print(" "); //if not print a space
              }
            }
            else{
              if(f == (size/2) + 1){
                  System.out.print("|"); //if box dimensions are odd and f is the middle of the box, print |
                }
              else{
                System.out.print(" "); //if not print a space
              }
            }
            j++; //increment counter that changes remainder
          }
          System.out.println("");
        }
        x++; //increment counter that changes remainder
      }
    }
}