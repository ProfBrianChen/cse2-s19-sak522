//Shant Keshishian
//CSE 002
//Lab 07
//4/5/19

//This program will create random sentences
import java.util.Scanner; //Import scanner to enable user input
import java.util.Random; //Import class to generate random integer

public class Methods{ // Class required for every Java program
  public static String Adjective(){ //method for adjectives in sentences
    Random randomGenerator = new Random(); //create random number generator 
    int rand = randomGenerator.nextInt(10); //Generates random integers less than 10
    String word = ""; //this variable stores the random adjective
    
    switch(rand){ //switch statement that determines which word to use
        case 0:
          word = "magenta";
          break;
        case 1:
          word = "ugly";
          break;
        case 2:
          word = "chewy";
          break;
        case 3:
          word = "fat";
          break;
        case 4:
          word = "bloody";
          break;
        case 5:
          word = "bald";
          break;
        case 6:
          word = "spicy";
          break;
        case 7:
          word = "cold";
          break;
        case 8:
          word = "green";
          break;
        case 9:
          word = "gargantuan";
          break;
    } 
    return word; //returns string to be used in sentence in main method
  }

  public static String Verb(){ //method for verbs in sentences
    Random randomGenerator = new Random(); //create random number generator
    int rand = randomGenerator.nextInt(10); //Generates random integers less than 10
    String word = ""; //this variable stores the random adjective

    switch(rand){ //switch statement that determines which word to use
        case 0:
          word = "murdered";
          break;
        case 1:
          word = "choked";
          break;
        case 2:
          word = "consumed";
          break;
        case 3:
          word = "baked";
          break;
        case 4:
          word = "destroyed";
          break;
        case 5:
          word = "threw";
          break;
        case 6:
          word = "rode";
          break;
        case 7:
          word = "drank";
          break;
        case 8:
          word = "broke";
          break;
        case 9:
          word = "ate";
          break;
    } 
    return word; //returns string to be used in sentence in main method
  }
  
public static String Subject(){ //method for subject in sentences
    Random randomGenerator = new Random(); //create random number generator
    int rand = randomGenerator.nextInt(10); //Generates random integers less than 10
    String word = ""; //this variable stores the random adjective
    
    switch(rand){ //switch statement that determines which word to use
        case 0:
          word = "fox";
          break;
        case 1:
          word = "dude";
          break;
        case 2:
          word = "man";
          break;
        case 3:
          word = "lady";
          break;
        case 4:
          word = "cow";
          break;
        case 5:
          word = "frog";
          break;
        case 6:
          word = "rhino";
          break;
        case 7:
          word = "sasquatch";
          break;
        case 8:
          word = "dog";
          break;
        case 9:
          word = "cat";
          break;
    } 
    return word; //returns string to be used in sentence in main method
  }

public static String Obj(){ //method for objects in sentences
    Random randomGenerator = new Random(); //create random number generator
    int rand = randomGenerator.nextInt(10); //Generates random integers less than 10
    String word = ""; //this variable stores the random adjective
    
    switch(rand){ //switch statement that determines which word to use
        case 0:
          word = "brick";
          break;
        case 1:
          word = "rock";
          break;
        case 2:
          word = "wallet";
          break;
        case 3:
          word = "bed";
          break;
        case 4:
          word = "fridge";
          break;
        case 5:
          word = "laptop";
          break;
        case 6:
          word = "bottle";
          break;
        case 7:
          word = "phone";
          break;
        case 8:
          word = "box";
          break;
        case 9:
          word = "lamp";
          break;
    } 
    return word; //returns string to be used in sentence in main method
  }
  public static String Sentence1(){
    String adj = Adjective();
    String verb = Verb();
    String subject = Subject();
    String obj = Obj(); 
    
    System.out.println("The " + adj + " " + subject + " " + verb + " the " + obj); 
    return subject;
   }  
  
  public static void Sentence2(String subject2){
    Random randomGenerator = new Random(); //create random number generator
    int rand1 = randomGenerator.nextInt(10); //number of body sentences
    String word = ""; //this variable stores the random adjective
    for(int i = 0; i < rand1; i++){ //creates for loop to create multiple sentences depending on the random integer
      int rand2 = randomGenerator.nextInt(2); //Generates random integers less than 10
      int rand3 = randomGenerator.nextInt(2);
      String adj = Adjective();
      String verb = Verb();
      String subject = Subject();
      String obj = Obj(); 

      switch(rand2){ //switch statement to decide whether to is "it" or the subject
        case 0:
          word = "It";
          break;
        case 1:
          word = "This " + subject2;
          break;
      }
      switch(rand3){//switch statement that to decide what sentence structure to use
        case 0:
          System.out.println(word + " used a " + obj + " and " + verb + " the " + adj + " " + subject); //print this sentence half of the time
          break;
        case 1:
          System.out.println(word + " had a " + adj + " " + subject); //print this sentence half of the time
          break;
      }
      
    }
  }
  
  public static void Sentence3(String subject2){
    Random randomGenerator = new Random(); //create random number generator
    int rand = randomGenerator.nextInt(2); //Generates random integers less than 10
    String word = ""; //this variable stores the random adjective
    
    String adj = Adjective();
    String verb = Verb();
    String obj = Obj(); 
    
    System.out.println("The " + subject2 + " " + verb + " his " + obj + "!"); //print the sentence
  }
  
  public static void Combine(String subject){ //method that combines the sentences together
   Sentence2(subject); //calls the method that creates the body sentences using the subject from main method
   Sentence3(subject); //calls the method that creates the conclusion sentence using the subject from main method
  }
  
  
    public static void main(String[] args) { // Main method required for every Java program
      Scanner myScanner = new Scanner(System.in); //Create your own Scanner using the first line of code to import a Scanner
      String subject = "";
      subject = Sentence1();
      String junkWord = ""; //stores junk word in while loop
      while(true){ //creates infinite loop
        System.out.println("Would you like another sentence? (yes or no): ");
        if(myScanner.hasNext("no")){
          break; //break out of loop if user does would like to continue with this sentence
        }
        if(myScanner.hasNext("yes")){
          junkWord = myScanner.next();
          subject = Sentence1(); //store "yes" in junk and create a new initial sentence
        }
        else{
            System.out.println("ERROR!");
            junkWord = myScanner.next(); //if user inputs anything else, print error and store input in junk
          }
        }
        Combine(subject); //call the method to create the paragraph
      }
}