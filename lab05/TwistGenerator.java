//Shant Keshishian
//CSE 002
//Lab 05
//3/1/19

import java.util.Scanner; //add scanner to enable user input

// This program will print a simple "twist" on the screen
public class TwistGenerator{
   			public static void main(String[] args) { // Main method required for every Java program
          Scanner myScanner = new Scanner(System.in); //Create your own Scanner using the first line of code to import a Scanner
          
          System.out.print("Enter Positive Integer: "); //Prompts user to enter integer
          
          int num = 0; //Declare variable num
          int val = 0; //Declare variable val
            if(!myScanner.hasNextInt()){
              String junkWord = myScanner.next(); //store the incorrect input in a junk variable
              System.out.println("Not an Integer"); //print Not an Integer
            }
            else{ 
              num = myScanner.nextInt(); //if input is not junk, set num to the scanner variable
              if(!(num > 0)){ 
                System.out.println("Not Positive Integer"); //if num is less than 0, print Not Positive Integer
              }
              else{
                val = num; //if num is what we are looking for, set num equal to val
              }
            }
          
          int counter1 = 0; //Declare variable for first counter
          int cycle = val / 3; //amount of times 3 goes into val is amount of times we will repeat the while loop
            while(counter1 < cycle){
              System.out.print("\\ /");
              counter1++; //while the counter is less than the number of cycles, print
            }
              int rem1 = val % 3; //find remainder of val
                switch(rem1){ //use remainder to identify what needs to be printed after the loop is complete
                  case 1:
                    System.out.print("\\");
                    break;
                  case 2: 
                    System.out.print("\\  ");
                    break;
                }    
              System.out.println(""); //print new line
          
           int counter2 = 0; //Declare variable for second counter
         while(counter2 < cycle){
              System.out.print(" X ");
              counter2++; //while the counter is less than the number of cycles, print
            }
              int rem2 = val % 3; //find remainder of val
                switch(rem2){ //use remainder to identify what needs to be printed after the loop is complete for the second part of the twist
                  case 1:
                    System.out.print(" ");
                    break;
                  case 2: 
                    System.out.print(" X");
                    break;
                }    
          System.out.println(""); //print new line
          
            int counter3 = 0; //Declare variable for third counter
         while(counter3 < cycle){
              System.out.print("/ \\");
              counter3++; //while the counter is less than the number of cycles, print
            }
              int rem3 = val % 3; //find remainder of val
                switch(rem3){ //use remainder to identify what needs to be printed after the loop is complete for the last part of the twist
                  case 1:
                    System.out.print("/");
                    break;
                  case 2: 
                    System.out.print("/ ");
                    break;
           }
           System.out.println(""); //print new line
}
}