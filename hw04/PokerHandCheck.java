//Shant Keshishian
//CSE 002
//HW 04
//2/19/19

// This program will randomly select 5 cards out of 5 decks of 52 cards and print if there is a pair, two pairs, three of a kind, or a high card
public class PokerHandCheck{ 
   			public static void main(String[] args) { // Main method required for every Java program
         
          int num1 = (int) (Math.random() * 50) + 2;
          int num2 = (int) (Math.random() * 50) + 2;
          int num3 = (int) (Math.random() * 50) + 2;
          int num4 = (int) (Math.random() * 50) + 2;
          int num5 = (int) (Math.random() * 50) + 2;
          //Declare 5 variable for the 5 randomly drawn cards
          
          System.out.println("Your Random Cards are: "); //Prints out phrase "Your Random Cards are: "
          
        String suit1;   // Declare string variable for the suit of the card
        if (num1 <= 13){
          suit1 = "Diamonds"; // If statement assigns the suit variable the string Diamonds if the random integer is less than 13
        }
          else if (num1 > 13 && num1 <= 26){
            suit1 = "Clubs"; // If statement assigns the suit variable the string Clubs if the random integer is more than 13 and less than 26
          }
            else if (num1 > 26 && num1 <= 39){
              suit1 = "Hearts"; // If statement assigns the suit variable the string Hearts if the random integer is more than 26 and less than 39
            }
              else {
                suit1 = "Spades"; // If statement assigns the suit variable the string Spades for any other condition 
              }
          
         int card1 = num1 % 13; // Divides random integer by 13 and stores remainder as variable
         String cardName1; // Declares new string variable for the name of a royal card
         switch(card1){
           case 0: // Case for when remainder is 0
              cardName1 ="King"; // Store string King in this case
              System.out.println("The " + cardName1 + " of " + suit1); //Print out the card suit and name 
             break; // Exit switch loop
           case 1: // Case for when remainder is 1
                      // The case numbers do not necessarily correspond to any specific card, but they are the numbers that do not appear on cards
              cardName1 ="Queen"; // Store string Queen in this case
              System.out.println("The " + cardName1 + " of " + suit1); //Print out the card suit and name
             break; // Exit switch loop
           case 11: // Case for when remainder is 11
              cardName1 ="Jack"; // Store string Jack in this case
              System.out.println("The " + cardName1 + " of " + suit1); //Print out the card suit and name
             break; // Exit switch loop
           case 12: // Case for when remainder is 12
              cardName1 ="Ace"; // Store string Ace in this case
              System.out.println("The " + cardName1 + " of " + suit1); //Print out the card suit and name
             break; // Exit switch loop
           default: // If no case is true, run default 
              System.out.println("The " + card1 + " of " + suit1); //Print out the card suit and name using remainder value as name
             break; // Exit switch loop
         }
             
             
             String suit2;   // Declare string variable for the suit of the card
        if (num2 <= 13){
          suit2 = "Diamonds"; // If statement assigns the suit variable the string Diamonds if the random integer is less than 13
        }
          else if (num2 > 13 && num2 <= 26){
            suit2 = "Clubs"; // If statement assigns the suit variable the string Clubs if the random integer is more than 13 and less than 26
          }
            else if (num2 > 26 && num2 <= 39){
              suit2 = "Hearts"; // If statement assigns the suit variable the string Hearts if the random integer is more than 26 and less than 39
            }
              else {
                suit2 = "Spades"; // If statement assigns the suit variable the string Spades for any other condition 
              }
          
         int card2 = num2 % 13; // Divides random integer by 13 and stores remainder as variable
         String cardName2; // Declares new string variable for the name of a royal card
         switch(card2){
           case 0: // Case for when remainder is 0
              cardName2 ="King"; // Store string King in this case
              System.out.println("The " + cardName2 + " of " + suit2); //Print out the card suit and name 
             break; // Exit switch loop
           case 1: // Case for when remainder is 1
                      // The case numbers do not necessarily correspond to any specific card, but they are the numbers that do not appear on cards
              cardName2 ="Queen"; // Store string Queen in this case
              System.out.println("The " + cardName2 + " of " + suit2); //Print out the card suit and name
             break; // Exit switch loop
           case 11: // Case for when remainder is 11
              cardName2 ="Jack"; // Store string Jack in this case
              System.out.println("The " + cardName2 + " of " + suit2); //Print out the card suit and name
             break; // Exit switch loop
           case 12: // Case for when remainder is 12
              cardName2 ="Ace"; // Store string Ace in this case
              System.out.println("The " + cardName2 + " of " + suit2); //Print out the card suit and name
             break; // Exit switch loop
           default: // If no case is true, run default 
              System.out.println("The " + card2 + " of " + suit2); //Print out the card suit and name using remainder value as name
             break; // Exit switch loop
         }
             
             String suit3;   // Declare string variable for the suit of the card
        if (num3 <= 13){
          suit3 = "Diamonds"; // If statement assigns the suit variable the string Diamonds if the random integer is less than 13
        }
          else if (num3 > 13 && num3 <= 26){
            suit3 = "Clubs"; // If statement assigns the suit variable the string Clubs if the random integer is more than 13 and less than 26
          }
            else if (num3 > 26 && num3 <= 39){
              suit3 = "Hearts"; // If statement assigns the suit variable the string Hearts if the random integer is more than 26 and less than 39
            }
              else {
                suit3 = "Spades"; // If statement assigns the suit variable the string Spades for any other condition 
              }
          
         int card3 = num3 % 13; // Divides random integer by 13 and stores remainder as variable
         String cardName3; // Declares new string variable for the name of a royal card
         switch(card3){
           case 0: // Case for when remainder is 0
              cardName3 ="King"; // Store string King in this case
              System.out.println("The " + cardName3 + " of " + suit3); //Print out the card suit and name 
             break; // Exit switch loop
           case 1: // Case for when remainder is 1
                      // The case numbers do not necessarily correspond to any specific card, but they are the numbers that do not appear on cards
              cardName3 ="Queen"; // Store string Queen in this case
              System.out.println("The " + cardName3 + " of " + suit3); //Print out the card suit and name
             break; // Exit switch loop
           case 11: // Case for when remainder is 11
              cardName3 ="Jack"; // Store string Jack in this case
              System.out.println("The " + cardName3 + " of " + suit3); //Print out the card suit and name
             break; // Exit switch loop
           case 12: // Case for when remainder is 12
              cardName3 ="Ace"; // Store string Ace in this case
              System.out.println("The " + cardName3 + " of " + suit3); //Print out the card suit and name
             break; // Exit switch loop
           default: // If no case is true, run default 
              System.out.println("The " + card3 + " of " + suit3); //Print out the card suit and name using remainder value as name
             break; // Exit switch loop
         }
             
             String suit4;   // Declare string variable for the suit of the card
        if (num4 <= 13){
          suit4 = "Diamonds"; // If statement assigns the suit variable the string Diamonds if the random integer is less than 13
        }
          else if (num4 > 13 && num4 <= 26){
            suit4 = "Clubs"; // If statement assigns the suit variable the string Clubs if the random integer is more than 13 and less than 26
          }
            else if (num4 > 26 && num4 <= 39){
              suit4 = "Hearts"; // If statement assigns the suit variable the string Hearts if the random integer is more than 26 and less than 39
            }
              else {
                suit4 = "Spades"; // If statement assigns the suit variable the string Spades for any other condition 
              }
          
         int card4 = num4 % 13; // Divides random integer by 13 and stores remainder as variable
         String cardName4; // Declares new string variable for the name of a royal card
         switch(card4){
           case 0: // Case for when remainder is 0
              cardName4 ="King"; // Store string King in this case
              System.out.println("The " + cardName4 + " of " + suit4); //Print out the card suit and name 
             break; // Exit switch loop
           case 1: // Case for when remainder is 1
                      // The case numbers do not necessarily correspond to any specific card, but they are the numbers that do not appear on cards
              cardName4 ="Queen"; // Store string Queen in this case
              System.out.println("The " + cardName4 + " of " + suit4); //Print out the card suit and name
             break; // Exit switch loop
           case 11: // Case for when remainder is 11
              cardName4 ="Jack"; // Store string Jack in this case
              System.out.println("The " + cardName4 + " of " + suit4); //Print out the card suit and name
             break; // Exit switch loop
           case 12: // Case for when remainder is 12
              cardName4 ="Ace"; // Store string Ace in this case
              System.out.println("The " + cardName4 + " of " + suit4); //Print out the card suit and name
             break; // Exit switch loop
           default: // If no case is true, run default 
              System.out.println("The " + card4 + " of " + suit4); //Print out the card suit and name using remainder value as name
             break; // Exit switch loop
         }
             
             String suit5;   // Declare string variable for the suit of the card
        if (num5 <= 13){
          suit5 = "Diamonds"; // If statement assigns the suit variable the string Diamonds if the random integer is less than 13
        }
          else if (num5 > 13 && num5 <= 26){
            suit5 = "Clubs"; // If statement assigns the suit variable the string Clubs if the random integer is more than 13 and less than 26
          }
            else if (num5 > 26 && num5 <= 39){
              suit5 = "Hearts"; // If statement assigns the suit variable the string Hearts if the random integer is more than 26 and less than 39
            }
              else {
                suit5 = "Spades"; // If statement assigns the suit variable the string Spades for any other condition 
              }
          
         int card5 = num5 % 13; // Divides random integer by 13 and stores remainder as variable
         String cardName5; // Declares new string variable for the name of a royal card
         switch(card5){
           case 0: // Case for when remainder is 0
              cardName5 ="King"; // Store string King in this case
              System.out.println("The " + cardName5 + " of " + suit5); //Print out the card suit and name 
             break; // Exit switch loop
           case 1: // Case for when remainder is 1
                      // The case numbers do not necessarily correspond to any specific card, but they are the numbers that do not appear on cards
              cardName5 ="Queen"; // Store string Queen in this case
              System.out.println("The " + cardName5 + " of " + suit5); //Print out the card suit and name
             break; // Exit switch loop
           case 11: // Case for when remainder is 11
              cardName5 ="Jack"; // Store string Jack in this case
              System.out.println("The " + cardName5 + " of " + suit5); //Print out the card suit and name
             break; // Exit switch loop
           case 12: // Case for when remainder is 12
              cardName5 ="Ace"; // Store string Ace in this case
              System.out.println("The " + cardName5 + " of " + suit5); //Print out the card suit and name
             break; // Exit switch loop
           default: // If no case is true, run default 
              System.out.println("The " + card5 + " of " + suit5); //Print out the card suit and name using remainder value as name
             break; // Exit switch loop
         }
          boolean pair1 = card1 == card2; 
          boolean pair2 = card1 == card3; 
          boolean pair3 = card1 == card4;
          boolean pair4 = card1 == card5;
          boolean pair5 = card2 == card3;
          boolean pair6 = card2 == card4; 
          boolean pair7 = card2 == card5;
          boolean pair8 = card3 == card4;
          boolean pair9 = card3 == card5;
          boolean pair10 = card4 == card5;
          // Declare 10 boolean variables for all 10 possible pairs
          
          boolean pair = (pair1 || pair2 || pair3 || pair4 ||  pair5 ||  pair6 ||  pair7 ||  pair8 ||  pair9 ||  pair10);
          // Declare boolean variable that is true if any pair exists
          
          boolean twoPairs1 = pair1 && pair2;
          boolean twoPairs2 = pair1 && pair3;
          boolean twoPairs3 = pair1 && pair4;
          boolean twoPairs4 = pair1 && pair5;
          boolean twoPairs5 = pair1 && pair6;
          boolean twoPairs6 = pair1 && pair7;
          boolean twoPairs7 = pair1 && pair8;
          boolean twoPairs8 = pair1 && pair9;
          boolean twoPairs9 = pair1 && pair10;
          
          boolean twoPairs10 = pair2 && pair3;
          boolean twoPairs11 = pair2 && pair4;
          boolean twoPairs12 = pair2 && pair5;
          boolean twoPairs13 = pair2 && pair6;
          boolean twoPairs14 = pair2 && pair7;
          boolean twoPairs15 = pair2 && pair8;
          boolean twoPairs16 = pair2 && pair9;
          boolean twoPairs17 = pair2 && pair10;
         
          boolean twoPairs18 = pair3 && pair4;
          boolean twoPairs19 = pair3 && pair5;
          boolean twoPairs20 = pair3 && pair6;
          boolean twoPairs21 = pair3 && pair7;
          boolean twoPairs22 = pair3 && pair8;
          boolean twoPairs23 = pair3 && pair9;
          boolean twoPairs24 = pair3 && pair10;
          
          boolean twoPairs25 = pair4 && pair5;
          boolean twoPairs26 = pair4 && pair6;
          boolean twoPairs27 = pair4 && pair7;
          boolean twoPairs28 = pair4 && pair8;
          boolean twoPairs29 = pair4 && pair9;
          boolean twoPairs30 = pair4 && pair10;
          
          boolean twoPairs31 = pair5 && pair6;
          boolean twoPairs32 = pair5 && pair7;
          boolean twoPairs33 = pair5 && pair8;
          boolean twoPairs34 = pair5 && pair9;
          boolean twoPairs35 = pair5 && pair10;
          
          boolean twoPairs36 = pair6 && pair7;
          boolean twoPairs37 = pair6 && pair8;
          boolean twoPairs38 = pair6 && pair9;
          boolean twoPairs39 = pair6 && pair10;
          
          boolean twoPairs40 = pair7 && pair8;
          boolean twoPairs41 = pair7 && pair9;
          boolean twoPairs42 = pair7 && pair10;
          
          boolean twoPairs43 = pair8 && pair9;
          boolean twoPairs44 = pair8 && pair10;
          
          boolean twoPairs45 = pair9 && pair10;
          
          
          // Declare 45 boolean variables for all 45 possible double pairs
          
          boolean twoPairs = (twoPairs1 || twoPairs2 || twoPairs3 || twoPairs4 || twoPairs5 || twoPairs6 || twoPairs7 || twoPairs8 || twoPairs9 || twoPairs10 || twoPairs11 || twoPairs12 || twoPairs13 || twoPairs14 || twoPairs15 || twoPairs16 || twoPairs17 || twoPairs18 || twoPairs19 || twoPairs20 || twoPairs21 || twoPairs22 || twoPairs23 || twoPairs24 || twoPairs25 || twoPairs26 || twoPairs27 || twoPairs28 || twoPairs29 || twoPairs30 || twoPairs31 || twoPairs32 || twoPairs33 || twoPairs34 || twoPairs35 || twoPairs36 || twoPairs37 || twoPairs38 || twoPairs39 || twoPairs40 || twoPairs41 || twoPairs42 || twoPairs43 || twoPairs44 || twoPairs45);
          // Declare boolean variable that is true if any double pairs exist
          
          boolean threeKind1 =  card1 == card2 && card2 == card3;
          boolean threeKind2 =  card1 == card2 && card2 == card4;
          boolean threeKind3 =  card1 == card2 && card2 == card5;
          boolean threeKind4 =  card1 == card3 && card3 == card4;
          boolean threeKind5 =  card1 == card3 && card3 == card5;
          boolean threeKind6 =  card1 == card4 && card4 == card5;
          boolean threeKind7 =  card2 == card3 && card3 == card4;
          boolean threeKind8 =  card2 == card3 && card3 == card5;
          boolean threeKind9 =  card2 == card4 && card4 == card5;
          boolean threeKind10 =  card3 == card4 && card4 == card5;
          
          // Declare 10 boolean variables for all 10 possible three of a kind combinations
          
          boolean threeKind = (threeKind1 || threeKind2 || threeKind3 || threeKind4 || threeKind5 || threeKind6 || threeKind7 || threeKind8 || threeKind9 || threeKind10);
          // Declare boolean variable that is true if any three of a kind exists
          
          if(threeKind){
            System.out.println("You Have Three of a Kind!"); // Print this if there are three of a kind
          }
            else if (twoPairs){
              System.out.println("You Have Two Pairs!"); // Print this is there are 2 pairs
            }
              else if (pair){
                System.out.println("You Have a Pair!"); // Print if there is only 1 pair
              }
               else{
                  System.out.println("You Have a High Card!"); //Print for all other scenarios
                }
        } // End of main method
} // End of class