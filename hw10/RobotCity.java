//Shant Keshishian
//CSE 002
//5/1/19

import java.util.Random; //import class to create random generator

public class RobotCity{
  
  public static int[][] buildCity(){ //method to create city array
    Random randomGenerator = new Random(); 
    int rand1 = randomGenerator.nextInt(6)+10; //create rand int to be x dimension of array
    int rand2 = randomGenerator.nextInt(6)+10; //create rand int to be y dimension of array
    int[][] array = new int[rand1][rand2]; //create array
    for(int i = 0; i < array.length; i++){
      for(int j = 0; j < array[i].length; j++){
        int rand3 = randomGenerator.nextInt(900)+100;
        array[i][j] = rand3; //give array members their values 100-999
      }
    }
    return array; //return array to main method
  }

  public static void display(int[][] array){ //print method
    for(int i = 0; i < array.length; i++){
      for(int j = 0; j < array[i].length; j++){
        System.out.printf("%5s",array[i][j]); //prints array members using printf() so that they are evenly spaced
      }
      System.out.println();
      System.out.println();
    }  
  }
  
  public static void invade(int[][] array, int k){ //method that creates the robot and puts them in the city array
    Random randomGenerator = new Random();
    System.out.println("k = " +k);
    System.out.println();
    for(int i = 0; i < k; i++){ //loop k times
      int rand1 = randomGenerator.nextInt(array.length); 
      int rand2 = randomGenerator.nextInt(array[rand1].length); //select random point in array
      while(array[rand1][rand2] < 0){ //while array member is invaded, find a new random array member to invade
        rand1 = randomGenerator.nextInt(array.length); 
        rand2 = randomGenerator.nextInt(array[rand1].length); 
      }
      array[rand1][rand2] = -(array[rand1][rand2]); //make random member value negative
    }
  }
  
  public static void update(int[][] array){ //method to update position of robots in array
    for(int i = 0; i < array.length; i++){
      for(int j = 0; j < array[i].length; j++){
        if(array[i][j] < 0){ //checks if array member is negative
          if(j == 0){ //checks if the robot is at the end of the array
            array[i][j] = -(array[i][j]); //makes the member values positive
          }
          else{
            array[i][j-1] = -(array[i][j-1]); //make the member to the left negative
            array[i][j] = -(array[i][j]); //make current member positive again
          }
        }
      }
    }
  }

  public static void main(String [] args){
    System.out.println();
    System.out.println();
    int[][] cityArray = buildCity(); //let output of buildCity() be the cityArray
    display(cityArray); //print the array
    System.out.println();
    System.out.println();
    Random randomGenerator = new Random(); 
    int k = randomGenerator.nextInt(21)+10; //create random number of robots
    invade(cityArray,k); //run method to release robots
    display(cityArray); //print the array
    System.out.println();
    for(int i = 0; i < 5; i++){ //update and print array 5 times
      update(cityArray);
      System.out.println();
      display(cityArray);
      System.out.println();
    }
  }
}  