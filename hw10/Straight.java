//Shant Keshishian
//CSE 002
//5/1/19

import java.util.Scanner; //import class to create a scanner
import java.util.Random; //import class to create a random number generator
import java.util.Arrays; //import class to sort arrays

public class Straight{
  
  public static int[] Deck(){ //method to create deck array
    Random randomGenerator = new Random();  
    int[] array = new int[52]; //create array
    for(int i = 0; i < array.length; i++){
      array[i] = i; //initialize array
    }
    
    for(int j = 0; j < array.length; j++){
      int target = (int) ((array.length)*(Math.random()));
      
      int temp = array[target];
	    array[target] = array[j];
	    array[j] = temp; //swap the members of the deck array
    }
    
//    System.out.println();
    return array; //return array to main method
  }
  
  public static int[] Draw(int[] array){ //method to create a hand
    int[] hand = new int[5];
    for(int i = 0; i < hand.length; i++){
      hand[i]= array[i]; //Create the hand to be analyzed
//      System.out.print(i+ " ");
  //    System.out.println(array[i]);
    }
    return hand;
  }
  
  public static int lowCard(int k, int[] hand){ //method to find kth lowestmember
    if(k > 4 || k < 0){
      return -1; //return -1 if does not fall intor ight category
    }
    Arrays.sort(hand); //sort the array from smallest to largest
    return hand[k]; //return the kth lowest card
  }
  
  public static int straight(int[]hand, int k){ //method to find straight
    int low = lowCard(k, hand); //call on method to find kth lowest member
    int count =0;
    if((hand[1] % 13 == (hand[0] + 1) % 13) && (hand[2] % 13 == (hand[0] +2) % 13) && (hand[3] % 13 == (hand[0] +3) % 13) && (hand[4] % 13 == (hand[0] +4) % 13)){
      count++; //if the condition is satisfied, increment the count
    }
    return count;
  }
  
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Enter k Value: ");
    int k = myScanner.nextInt();
    int count =0;
    for(int i = 0; i < 1000000; i++){ //loop a million times
      int[] deckArray = Deck(); // create a new deck
      int[] hand = Draw(deckArray); //create a new hand
      Arrays.sort(hand); //sort the array
      int temp = straight(hand,k); //check for a straight and store count
      count = temp + count; //sum of counts here
    }
     System.out.println(count); //print sum of counts
}
}
  
