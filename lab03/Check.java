//Shant Keshishian
//CSE 002
//Lab 03
//2/8/19

import java.util.Scanner; //add scanner to enable user input

public class Check{
   			public static void main(String[] args) { // main method required for every Java program
          
       Scanner myScanner = new Scanner( System.in ); //Create your own Scanner using the first line of code to import a Scanner
          
          System.out.print("Enter the original cost of the check in the form xx.xx: "); //Prompts user to enter cost of dinner
          double checkCost = myScanner.nextDouble(); //Creates variable for the total cost the user inputs
          
          System.out.print("Enter the percentage tip that you wish to pay as a whole number in the form xx:" ); //Prompts user to enter total tip
          double tipPercent = myScanner.nextDouble(); //Creates variable for tip value the user inputs
          tipPercent /= 100; //We want to convert the percentage into a decimal value
            
          System.out.print("Enter the number of people who went out to dinner:"); //Prompts user to input number of people at dinner
          int numPeople = myScanner.nextInt(); //Create variable for the amount of people at dinner inputed

          double totalCost; //defines new variable for cost plus tip
          double costPerPerson; // defines new variable for the cost per person
          int dollars,   //whole dollar amount of cost 
          dimes, pennies; //for storing digits to the right of the decimal point for the cost$ 
          
totalCost = checkCost * (1 + tipPercent); // Adds tip amount to original check cost
costPerPerson = totalCost / numPeople; //get the whole amount, dropping decimal fraction, per person
dollars=(int)costPerPerson; //Creates variable for dollar amount in the final cost per person
dimes=(int)(costPerPerson * 10) % 10; 
pennies=(int)(costPerPerson * 100) % 10;
// Above gets dimes and pennies amounts, e.g., 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //Print final cost per person


}  //end of main method   
  	} //end of class