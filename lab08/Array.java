//Shant Keshishian
//CSE 002
//Lab 08
//4/5/19

//This program will create random sentences
import java.util.Random; //Import class to generate random integer
import java.text.DecimalFormat; //add DecimalFormat class to easily change amount of decimal places in final answer
import java.util.Arrays; //import class to sort arrays

public class Array{ //Class needed for every java program
  
  public static int getRange(int[] array){
    Arrays.sort(array); //sort arrays from smallest to largest
    return array[array.length-1] - array[0]; //return the range of the array to main method
  }
  
  public static double getMean(int[] array){ //Method to get the mean
    Arrays.sort(array); //sort arrays from smallest to largest
    int sum = 0; //initialize variable sum
    double mean = 0; //initialize variable mean
    for(int i = 0; i < array.length; i++){
      sum += array[i]; //loop to add value of array to sum variable
    }
    mean = (sum / array.length); //find the mean using sum and length of array
    return mean; //return the mean to main method
  }
  
  public static double getStdDev(double mean, int[] array){ //Method to get the standard deviation
    double sum = 0;
    for(int i = 0; i < array.length; i++){
      sum += (array[i] - mean) * (array[i] - mean); //loop that adds the array value minus the mean squared to the variable sum
    }
    double stdDev = Math.sqrt(sum/(array.length-1));
    return stdDev; //return the standard deviation to main method
  }
  
  public static void shuffle(int[] array){ //Method to shuffle the members of the array
    for(int i = 0; i < array.length; i++){ //loop to assign values to array
      int val = (int) (array.length * Math.random()); //chooses random array member
      int temp = array[val]; //temp variable stores value
      array[val] = array[i]; //give that array member a new value
      array[i] = temp; //let the other member take the value of temp
      System.out.println("[" + i + "]" + " " + array[i]);
    }
  }
  
  public static void main(String[] args){ //Main Method needed for every java Program
    Random randomGenerator = new Random(); //create random number generator
    int rand = (randomGenerator.nextInt(51)) + 50; //Generates random integers greater than 50 and less than 100
    int[] array = new int[rand]; //create an array with random size
    System.out.println("Array Size = " + rand);
    for(int i = 0; i < array.length; i++){
      array[i] = randomGenerator.nextInt(100);
      System.out.println("[" + i + "]" + " " + array[i]); //loop assigns values 0-100 to array members
    }
    int range = getRange(array); //store returned value from method
    double mean = getMean(array); //store returned value from method
    double stdDev = getStdDev(mean, array); //store returned value from method
    System.out.println("Range: " + range); //sprint returned value from method
    System.out.println("Mean: " + mean); //print returned value from method
    DecimalFormat numberFormat = new DecimalFormat("#.000"); //let standard deviation only have 3 digits after decimal point
    System.out.println("Standard Deviation: " + numberFormat.format(stdDev)); //print returned value from method
    shuffle(array); //call on method to shuffle the array
  }
}
  