//Shant Keshishian
//CSE 002
//HW 08

//This program will create random sentences
import java.util.Random; //Import class to generate random integer
import java.util.Scanner; //Import class to create scanner

public class PlayLottery{ //Class required for java program
  public static void main(String args []){
    Scanner myScanner = new Scanner( System.in ); //Create your own Scanner using the first line of code to import a Scanner
    
    int guess1 = 0;
    int guess2 = 0;
    int guess3 = 0;
    int guess4 = 0;
    int guess5 = 0; //initialize the vvariables that will be users guess for lottery
    
    while(true){ //loop through until conditions are met
       int val1=0;
       System.out.print("Enter Guess #1: "); //Prompts user to enter width
      if(myScanner.hasNextInt()){
        val1 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
        if(val1 > 60 || val1 < 0){
          System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
        }
        else{
          guess1 = val1; 
          break; //if conditions are met, set val1 to length and break out of infinite loop
        }
      }
      else{
        System.out.println("ERROR!: ");
        String junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
      }
     }
    
    while(true){ //loop through until conditions are met
       int val2=0;
       System.out.print("Enter Guess #2: "); //Prompts user to enter width
      if(myScanner.hasNextInt()){
        val2 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
        if(val2 > 60 || val2 < 0){
          System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
        }
        else{
          guess2 = val2; 
          break; //if conditions are met, set val1 to length and break out of infinite loop
        }
      }
      else{
        System.out.println("ERROR!: ");
        String junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
      }
     }
    
    while(true){ //loop through until conditions are met
       int val3=0;
       System.out.print("Enter Guess #3: "); //Prompts user to enter width
      if(myScanner.hasNextInt()){
        val3 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
        if(val3 > 60 || val3 < 0){
          System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
        }
        else{
          guess3 = val3; 
          break; //if conditions are met, set val1 to length and break out of infinite loop
        }
      }
      else{
        System.out.println("ERROR!: ");
        String junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
      }
     }
    
    while(true){ //loop through until conditions are met
       int val4=0;
       System.out.print("Enter Guess #4: "); //Prompts user to enter width
      if(myScanner.hasNextInt()){
        val4 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
        if(val4 > 60 || val4 < 0){
          System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
        }
        else{
          guess4 = val4; 
          break; //if conditions are met, set val1 to length and break out of infinite loop
        }
      }
      else{
        System.out.println("ERROR!: ");
        String junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
      }
     }
    
    while(true){ //loop through until conditions are met
       int val5=0;
       System.out.print("Enter Guess #5: "); //Prompts user to enter width
      if(myScanner.hasNextInt()){
        val5 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
        if(val5 > 60 || val5 < 0){
          System.out.println("ERROR!: "); //if the input is negative, pringt ERROR!
        }
        else{
          guess5 = val5; 
          break; //if conditions are met, set val1 to length and break out of infinite loop
        }
      }
      else{
        System.out.println("ERROR!: ");
        String junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
      }
     }
   
    int[] guess = { guess1, guess2, guess3, guess4, guess5 }; //Sets guessing array using user input
    
    Random randomGenerator = new Random(); //create random generator 
    int[] win = new int[5];
    for(int i = 0; i < 5; i++){
      win[i] = (randomGenerator.nextInt(60));
      System.out.print(win[i] + " "); //loop that assigns random number 0-60 to array members
    }
    System.out.println("");
    
    if(win[0] == guess1 && win[1] == guess2 && win[2] == guess3 && win[3] == guess4 && win[4] == guess5){
      System.out.println("Congratulations, You Won the Lottery!"); //If all conditions are met, you win!
    }
    else{
      System.out.println("Sorry You Lose!"); //If not, you lose
    }
  }
}
