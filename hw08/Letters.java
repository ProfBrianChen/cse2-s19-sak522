//Shant Keshishian
//CSE 002
//HW 08

//This program will create random sentences
import java.util.Random; //Import class to generate random integer

public class Letters{ //Class required for java program
  
  public static char[] getAtoM(char[] array){ //Method for A-M
    int counter1 = 0; //create a counter for loop #1
    for(int i = 0; i < array.length; i++){
      if(array[i] == 'a' || array[i] == 'b' || array[i] == 'c' || array[i] == 'd' || array[i] == 'e' || array[i] == 'f' || array[i] == 'g' || array[i] == 'h' || array[i] == 'i' || array[i] == 'j' || array[i] == 'k' || array[i] == 'l' || array[i] == 'm' || array[i] == 'A' || array[i] == 'B' || array[i] == 'C' || array[i] == 'D' || array[i] == 'E' || array[i] == 'F' || array[i] == 'G' || array[i] == 'H' || array[i] == 'I' || array[i] == 'J' || array[i] == 'K' || array[i] == 'L' || array[i] == 'M'){
        counter1++; //loop that increments counter everytime if statement is satisfied to create size of new array
      }
    }
    char[] newArray = new char[counter1]; //create new array using size from other loop
    int counter2 = 0; //create a counter for loop #2
    for(int j = 0; j < array.length; j++){
      if(array[j] == 'a' || array[j] == 'b' || array[j] == 'c' || array[j] == 'd' || array[j] == 'e' || array[j] == 'f' || array[j] == 'g' || array[j] == 'h' || array[j] == 'i' || array[j] == 'j' || array[j] == 'k' || array[j] == 'l' || array[j] == 'm' || array[j] == 'A' || array[j] == 'B' || array[j] == 'C' || array[j] == 'D' || array[j] == 'E' || array[j] == 'F' || array[j] == 'G' || array[j] == 'H' || array[j] == 'I' || array[j] == 'J' || array[j] == 'K' || array[j] == 'L' || array[j] == 'M'){
        newArray[counter2] = array[j];
        counter2++; //This loop will now actually store the values that satisfy the if statement
      }
    }
    return newArray; //return output to main method
  }
  
  public static char[] getNtoZ(char[] array){ //Method for N-Z
     int counter1 = 0;//create a counter for loop #1
     for(int i = 0; i < array.length; i++){
       if(array[i] == 'n' || array[i] == 'o' || array[i] == 'p' || array[i] == 'q' || array[i] == 'r' || array[i] == 's' || array[i] == 't' || array[i] == 'u' || array[i] == 'v' || array[i] == 'w' || array[i] == 'x' || array[i] == 'y' || array[i] == 'z' || array[i] == 'N' || array[i] == 'O' || array[i] == 'P' || array[i] == 'Q' || array[i] == 'R' || array[i] == 'S' || array[i] == 'T' || array[i] == 'U' || array[i] == 'V' || array[i] == 'W' || array[i] == 'X' || array[i] == 'Y' || array[i] == 'Z' ){
         counter1++; //loop that increments counter to create size of new array
       }
     }
     char[] newArray = new char[counter1]; //create array that will store characters
     int counter2 = 0; //create a counter for loop #2
     for(int j = 0; j < array.length; j++){
       if(array[j] == 'n' || array[j] == 'o' || array[j] == 'p' || array[j] == 'q' || array[j] == 'r' || array[j] == 's' || array[j] == 't' || array[j] == 'u' || array[j] == 'v' || array[j] == 'w' || array[j] == 'x' || array[j] == 'y' || array[j] == 'z' || array[j] == 'N' || array[j] == 'O' || array[j] == 'P' || array[j] == 'Q' || array[j] == 'R' || array[j] == 'S' || array[j] == 'T' || array[j] == 'U' || array[j] == 'V' || array[j] == 'W' || array[j] == 'X' || array[j] == 'Y' || array[j] == 'Z' ){
         newArray[counter2] = array[j];
         counter2++; //This loop will now actually store the values that satisfy the if statement
       }
     }
     return newArray; //return output to main method
  }
  
  public static void main(String[] args){ //Main method required for program
    Random randomGenerator = new Random(); //create random generator
    String alphabet = "ABCDEFGHIJKLMabcdefghijklmNOPQRSTUVWXYZnopqrstuvwxyz"; //create string that program can create array out of
    int randSize = (randomGenerator.nextInt(51)) + 10; // set bounds for array that is 10-50 characters long
    char[] array = new char[randSize]; //create array with random size
    System.out.print("Random String: ");
    for(int i = 0; i < array.length; i++){
      array[i] = (alphabet.charAt(randomGenerator.nextInt(alphabet.length())));
      System.out.print(array[i]); //loop that assigns characters to array members using string "alphabet"
    }
    System.out.println("");
    char[] am = getAtoM(array); //run method A-M and store output
    char[] nz = getNtoZ(array); //run method N-Z and store output
    System.out.print("Characters A-M: ");
    System.out.println(am);
    System.out.print("Characters N-Z: ");
    System.out.println(nz);
  }
}
  