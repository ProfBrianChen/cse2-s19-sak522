//Shant Keshishian
//CSE 002
//HW09

import java.util.Random; //import class to make random number generator
import java.util.Scanner; //import class to create scanner

public class ArrayGames{ //class needed for java program
  
  public static int[] generate(){ //method to create random array
    Random randomGenerator = new Random(); //create random number generator
    int rand = randomGenerator.nextInt(11) + 10; //generate numbers 10-20
    int[] array = new int[rand]; //create array of random size
    for(int i = 0; i < array.length; i++){
      array[i] = randomGenerator.nextInt(51); //loop that gives array its values
    }
    return array; //return this array to main method
  }
  
  public static void print(int[] array){ //method to print inputs
    System.out.print("{");
    for(int i = 0; i < array.length; i++){
      System.out.print(array[i] + ","); //loop that prints out each array member 
    }
    System.out.println("}");
  }
  
  public static void insert(int[] array1, int[] array2){ //method to insert array into other array
    int[] bigArray = new int[array1.length + array2.length]; //create new array size of both arrays combined
    Random randomGenerator = new Random(); //create random number generator
    int rand = randomGenerator.nextInt(array1.length+1); //generate random number less than size of array1
    for(int i = 0; i < rand; i++){ //loop to assign values to big array
          bigArray[i] = array1[i]; //set bigarray members equal to array1 members
        }
    for(int j = rand; j < (array2.length + rand); j++){
        bigArray[j] = array2[j]; //set bigarray members starting at rand equal to array2 members
      }
    for(int k = rand; k < bigArray.length; k++){ //loop to assign values to big array
          bigArray[k+array2.length] = array1[k]; //set bigarray members equal to array1 members
        }
    print(bigArray); //call on print method to print array
  }
  
  public static void shorten(int[] array, int val){ //method to shorten array
    if(val > array.length){ //if val is larger than array size, then array[val] does not exist
      print(array); //call print method
    }
    else{ //if array[val] does exist
      int[] newArray = new int[array.length-1]; //create new array 1 memeber smaller than the last one
      for(int j = 0; j < val; j++){
        newArray[j] = array[j]; //assign the first part of array normally
      }
      for(int i = val; i+1 < array.length; i++){
        newArray[i] = array[i+1]; //now set the values of newarray to be one array member higher than array
      }
      print(newArray); //call on print method
    }
  }
  
  public static void main(String args []){ //main method needed for all java programs
    Scanner myScanner = new Scanner(System.in); //create a scanner
    while(true){ //infinite loop
      System.out.println("Would you like to run 'insert' or 'shorten'? "); //prompt reader on which method they want to run
      if(myScanner.hasNext("insert")){ //if they type insert
        System.out.print("Input1: ");
        int[] array1 = {0,1,2,3,4,5,6,7,8,9,10}; //call method to create random array
        print(array1); //call method to print array
        System.out.print("Input2: ");
        int[] array2 = {11,12,13,14,15,16,17,18,19,20,21};//call method to create random array
        print(array2); //call method to print array
        insert(array1, array2); //run insert
        break;
      }
      else if(myScanner.hasNext("shorten")){ //if they type shorten
        String junkword = myScanner.nextLine(); //store 'shorten' in junk
        System.out.println("Enter an Integer: ");
        int val = myScanner.nextInt(); //store input integer as val
        System.out.print("Input1: ");
        int[] array1 = generate(); //call method to create random array
        print(array1); //call method to print array
        shorten(array1, val); //run shorten
        break;
      }
      else{
        System.out.println("ERROR! Input Invalid");
        String junkWord = myScanner.nextLine(); //print error, store invalid input in junk and prompt user again
      }
    }
    
  }
}
