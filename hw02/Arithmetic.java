//Shant Keshishian
//CSE 002
//HW02
//2/6/19

//This program will find the total costs of each kind of item, the sales tax charged, and the total cost of all items
public class Arithmetic {
    // main method required for every Java program
    public static void main(String[] args) {

      int numPants = 3; //Number of pairs of pants
      double pantsPrice = 34.98;  //Cost per pair of pants
      int numShirts = 2; //Number of sweatshirts
      double shirtPrice = 24.99; //Cost per shirt
      int numBelts = 1; //Number of belts
      double beltPrice = 33.99;  //cost per belt
      double paSalesTax = 0.06;  //the tax rate 
      
      double costOfPants = (numPants * pantsPrice); //total cost of pants
      double costOfShirts = (numShirts * shirtPrice); //total cost of shirts
      double costOfBelts = (numBelts * beltPrice); //total cost of belts
     
      double taxPants = (paSalesTax * pantsPrice * numPants); //tax on "n" pairs of pants
      double taxShirts = (paSalesTax * shirtPrice * numShirts); //tax on "n" shirts
      double taxBelts = (paSalesTax * beltPrice * numBelts); // tax on "n" belts
      
      taxPants = taxPants * 100; //multiply the variable by 100
      int taxPantsInt = (int) taxPants; //convert the sales tax into an integer to get rid of extra decimal places
      double taxPantsDub = (double) taxPantsInt / 100; //convert it back to a double so that we can divide it by 100 and restore its original value
      
      taxShirts = taxShirts * 100; //multiply the variable by 100
      int taxShirtsInt = (int) taxShirts; //convert the sales tax into an integer to get rid of extra decimal places
      double taxShirtsDub = (double) taxShirtsInt / 100; //convert it back to a double so that we can divide it by 100 and restore its original value
      
      taxBelts = taxBelts * 100; //multiply the variable by 100
      int taxBeltsInt = (int) taxBelts; //convert the sales tax into an integer to get rid of extra decimal places
      double taxBeltsDub = (double) taxBeltsInt / 100; //convert it back to a double so that we can divide it by 100 and restore its original value
     
      double totalCost = (costOfPants + costOfShirts + costOfBelts); //Finds the sum of the costs of each kind of item
      double totalTax = (taxPantsDub + taxBeltsDub + taxShirtsDub); //Finds the sum of the sales tax of each kind of item using the newly adjusted values
      double totalCostWithTax = (totalTax + totalCost); //Finds the sum of the costs of each kind of item and the sales tax of each kind of item
      
      System.out.println("The Total Cost of Pants Without Tax is "+"$"+(costOfPants)); //Print the variable costOfPants
      System.out.println("The Total Cost of Shirts  Without Tax is "+"$"+(costOfShirts)); //Print the variable costOfShirts
      System.out.println("The Total Cost of Belts Without Tax is "+"$"+(costOfBelts)); //Print the variable costOfBelts
      
      System.out.println("The Sales Tax On Pants is "+"$"+(taxPantsDub)); //Print the variable taxPants
      System.out.println("The Sales Tax On Shirts is "+"$"+(taxShirtsDub)); //Print the variable taxShirts
      System.out.println("The Sales Tax On Belts is "+"$"+(taxBeltsDub)); //Print the variable taxBelts
      
      System.out.println("The Total Cost Without Tax is "+"$"+(totalCost)); 
      //Print the sum of the costs of each kind of item
      System.out.println("The Total Sales Tax is "+"$"+(totalTax));
      //Print the sum of the sales tax of each kind of item using the newly adjusted values
      System.out.println("The Total Cost With Tax is "+"$"+(totalCostWithTax)); 
      //Print the sum of the costs of each kind of item and the sales tax of each kind of item
      
  } //end of main method
} //end of class Arithmetic
