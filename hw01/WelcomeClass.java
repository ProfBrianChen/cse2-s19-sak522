//Shant Keshishian
//CSE 002 Welcome Class
//
public class WelcomeClass{
  
  public static void main(String args[]){
    ///Prints the box with Welcome inside
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
      //Prints username
    System.out.println("  ^  ^  ^  ^  ^  ^");
      //the slash is an escape character, therefore it must be escaped by itself
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-S--A--K--5--5--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    }
    
  }

  
    
