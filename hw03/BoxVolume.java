//Shant Keshishian
//CSE 002
//Hw03 Program#2
//2/12/19

import java.util.Scanner; //add scanner to enable user input

public class BoxVolume{
   			public static void main(String[] args) { // main method required for every Java program
          
          Scanner myScanner = new Scanner( System.in ); //Create your own Scanner using the first line of code to import a Scanner
          
          System.out.print("Enter width of box: "); //Prompts user to enter width
          double width = myScanner.nextDouble(); //Creates variable for width
          
          System.out.print("Enter length of box: "); //Prompts user to enter length
          double length = myScanner.nextDouble(); //Creates variable for length
          
          System.out.print("Enter height of box: "); //Prompts user to enter height
          double height = myScanner.nextDouble(); //Creates variable for height
          
          double volume = width * length * height; //multiply 3 variable together to get volume
          System.out.println("The Volume of the Box is " +volume); //print the final converted answer
        }//end of main method  
}//end of class