//Shant Keshishian
//CSE 002
//Hw03 Program#1
//2/12/19

import java.util.Scanner; //add scanner to enable user input
import java.text.DecimalFormat; //add DecimalFormat class to easily change amount of decimal places in final answer

public class Convert{
   			public static void main(String[] args) { // main method required for every Java program
          
          Scanner myScanner = new Scanner( System.in ); //Create your own Scanner using the first line of code to import a Scanner
          
          System.out.print("Enter distance in meters: "); //Prompts user to enter distancve in meters
          double distanceMeter = myScanner.nextDouble(); //Creates variable for distance using user input
          
          double conversion = 39.3701; //Creat conversion rate variable to multiply by meter distance
          double distanceInch = distanceMeter * conversion; //multiply meter distance by conversion rate
          
          DecimalFormat numberFormat = new DecimalFormat("#.0000"); //using DecimalFormat class, limit the ending decimals to 4
          System.out.println("Distance in inches is " + numberFormat.format(distanceInch)); //print the final converted answer
          
        }//end of main method  
}//end of class