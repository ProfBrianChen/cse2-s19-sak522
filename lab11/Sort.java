//Shant Keshishian
//CSE 002
//5/3/19

//NOT COMP

public class Sort{
  
  public static void main(String[] args){
    int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1};
		int iterBest1 = insertion(myArrayBest);
		int iterWorst1 = insertion(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest1);
    System.out.println();
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst1);
    System.out.println();
    int iterBest2 = selection(myArrayBest);
		int iterWorst2 = selection(myArrayWorst);
		System.out.println("The total number of operations performed on the sorted array: " + iterBest2);
    System.out.println();
		System.out.println("The total number of operations performed on the reverse sorted array: " + iterWorst2);
    System.out.println();
	}

  public static int insertion(int [] array){
    int n = array.length;  
    int count = 0;
    for (int j = 1; j < n; j++){  
      count++;
      int key = array[j];  
      int i = j-1;  
      while ((i > -1) && ( array [i] > key )){  
        array [i+1] = array [i];  
        i--;  
      }  
      array[i+1] = key;  
    }
    for(int i:array){    
      System.out.print(i+" ");    
    }
    System.out.println();
    return count;
  } 
  
  public static int selection(int[] array){
    int n = array.length;
    int count = 0;
    for (int i = 0; i < n - 1; i++){ 
      count++;
      int index = i;  
      for (int j = i + 1; j < n; j++){  
        if (array[j] < array[index]){  
          index = j;//searching for lowest index  
        }  
      }  
      int smallerNumber = array[index];   
      array[index] = array[i];  
      array[i] = smallerNumber;  
    } 
    for(int i:array){  
      System.out.print(i+" ");  
    } 
    System.out.println();
    return count;
    } 
}  
  


