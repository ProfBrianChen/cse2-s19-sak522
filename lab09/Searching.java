//Shant Keshishian
//CSE 002
//Lab09

import java.util.Scanner; //import class to create a scanner
import java.util.Random; //import class to create a random number generator
import java.util.Arrays; //import class to sort arrays

public class Searching{
  
  public static int[] MethodA(int input){
    Random randomGenerator = new Random(); //create random generator
    int[] array = new int[input]; //create new array
    for(int i = 0; i < input; i++){
      array[i] = (randomGenerator.nextInt(input+1));
      System.out.print(array[i]+ " "); //assign random values to array members and print array
    }
    return array; //return array to main method
  }
  
  public static int[] MethodB(int input){
    Random randomGenerator = new Random(); //create random generator
    int[] array = new int[input]; //create new array
    for(int i = 0; i < input; i++){
      array[i] = (randomGenerator.nextInt(input+1)); //assign random values to array members
    }
    Arrays.sort(array); //sort array 
    for(int i = 0; i < input; i++){
      System.out.print(array[i]+ " "); //print sorted array
    }
    System.out.println();
    return array; //return sorted array to main method
  }
  
  public static int MethodC(int[] array, int index){
    for(int i = 0; i < array.length; i++){
      if(array[i] == index){ //create loop that checks if array has a meber equal to input
        System.out.println();
        return i; //return the location of array member if case is satisfied
      }
    }
    System.out.println();
    return -1; //if value is not in array return -1
  }
  
  public static int MethodD(int[] array, int index, int low, int high){
    int mid = (low + high) / 2; //create middle point of array
    if (high < low) {
        return -1; //if the value is not in the array return -1
    }
    if (index == array[mid]) {
        return mid; //if the index is equal to the middle point, return the middle point
    } 
    else if (index < array[mid]) {
        return MethodD(array, index, low, mid - 1); //if index is smaller, then re-run this method, but make the high value the mid point minus 1
    } 
    else{
        return MethodD(array, index, mid + 1, high); //if index is bigger, then re-run this method, but make the low value the mid point plus 1
    }
  }
  
  public static void main(String args []){
    Scanner myScanner = new Scanner(System.in);
    int index1 =0;
    int index2 =0;
    int size1 =0;
    int size2 =0; //initialize and declare variables
    
    System.out.println("Would you like a Linear or Binary Search? ('linear' or 'binary')");
    while(true){
      if(myScanner.hasNext("linear")){
      String junk1 = myScanner.next();
      while(true){ //loop through until conditions are met
         int val1=0;
         System.out.print("Enter size of array: "); //Prompts user for input
         if(myScanner.hasNextInt()){
           val1 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
           if(val1 < 0){
             System.out.println("ERROR!: "); //if the input is negative, print ERROR!
           }
           else{
             size1 = val1; 
             break; //if conditions are met, set val1 to input and break out of infinite loop
           }
         }
        else{
          System.out.println("ERROR!: ");
          String junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
        }
       }
      while(true){ //loop through until conditions are met
         int val2=0;
         System.out.print("Enter search term: ");//Prompts user for input
         if(myScanner.hasNextInt()){
           val2 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
           if(val2 < 0){
             System.out.println("ERROR!: "); //if the input is negative, print ERROR!
           }
           else{
             index1 = val2; 
             break; //if conditions are met, set val to input and break out of infinite loop
           }
         }
        else{
          System.out.println("ERROR!: ");
          String junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
        }
       }
      int[] linearSearchArray = MethodA(size1); //create array in method A
      int outputC = MethodC(linearSearchArray, index1); //go to linear search method
      System.out.println(outputC); //print out linear search output
      break;
    }
     else if(myScanner.hasNext("binary")){
       String junk2 = myScanner.next();
       while(true){ //loop through until conditions are met
         int val3=0;
         System.out.print("Enter size of array: "); //Prompts user for input
         if(myScanner.hasNextInt()){
           val3 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
           if(val3 < 0){
             System.out.println("ERROR!: "); //if the input is negative, print ERROR!
           }
           else{
             size2 = val3; 
             break; //if conditions are met, set val to input and break out of infinite loop
           }
         }
        else{
          System.out.println("ERROR!: ");
          String junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
        }
       }
       while(true){ //loop through until conditions are met
         int val4=0;
         System.out.print("Enter search term: "); //Prompts user for input
         if(myScanner.hasNextInt()){
           val4 = myScanner.nextInt(); //set variable to scanner input if the input is an integer
           if(val4 < 0){
             System.out.println("ERROR!: "); //if the input is negative, print ERROR!
           }
           else{
             index2 = val4; 
             break; //if conditions are met, set val to input and break out of infinite loop
           }
         }
        else{
          System.out.println("ERROR!: ");
          String junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
        }
       }
       int[] binarySearchArray = MethodB(size2); //create sorted array in Method
       int outputD = MethodD(binarySearchArray, index2, binarySearchArray[0], binarySearchArray[binarySearchArray.length-1]); //go to binary search method
       System.out.println(outputD); //print out binary search output
       break;
    }
    else{
      String junk3 = myScanner.next();
      System.out.println("ERROR! Enter Valid String!"); //if input is not valid, print ERROR and store input in junk variable
    }
   }
  }
}