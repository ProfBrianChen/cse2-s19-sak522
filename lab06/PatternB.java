//Shant Keshishian
//CSE 002
//Lab 06
//3/8/19

import java.util.Scanner; //add scanner to enable user input

// This program will print a simple triangle formation
public class PatternB{
        public static void main(String[] args) { // Main method required for every Java program
          Scanner myScanner = new Scanner(System.in); //Create your own Scanner using the first line of code to import a Scanner
            int num = 0;
            int val = 0;
            String junkWord = ""; //Initialize variable to store incorrect string inputs
          while(true){ //loop through until conditions are met
            System.out.print("Enter Positive Integer (1-9): "); //Prompts user to enter integer
            if(myScanner.hasNextInt()){
              num = myScanner.nextInt(); // if input is an integer, set num variable equal to input
                if(num < 0 || num > 10){
                  System.out.println("ERROR!: "); // if num is not in range, print ERROR
                }
                else{
                  val = num;
                  break; // if all conditions are met, set num equal to val and break out of infinite loop
                }
            }
            else{
              System.out.println("ERROR!: ");
              junkWord = myScanner.next(); //if input is not an integer, print ERROR and store input in junk variable
            }
          }
          for(int i = 1; i <= val; i++){ //loop for the amount of rows in the pattern
            for(int j = 1; j < val - i + 2; j++){ //loop for what number to print
            System.out.print(j + " ");
            }
            System.out.println("");
          }
}
}