//Shant Keshishian
//CSE 002
//Lab 04
//2/15/19

// This program will randomly select a card out of a deck of 52 cards
public class CardGenerator{ 
   			public static void main(String[] args) { // Main method required for every Java program
         
        int num = (int) (Math.random() * 50) + 2; // Randomly select a number 0-50 and then add 2 to this value to disclude cards 0 and 1
        String suit;   // Declare string variable for the suit of the card
        if (num <= 13){
          suit = "Diamonds"; // If statement assigns the suit variable the string Diamonds if the random integer is less than 13
        }
          else if (num > 13 && num <= 26){
            suit = "Clubs"; // If statement assigns the suit variable the string Clubs if the random integer is more than 13 and less than 26
          }
            else if (num > 26 && num <= 39){
              suit = "Hearts"; // If statement assigns the suit variable the string Hearts if the random integer is more than 26 and less than 39
            }
              else {
                suit = "Spades"; // If statement assigns the suit variable the string Spades for any other condition 
              }
          
         int card = num % 13; // Divides random integer by 13 and stores remainder as variable
         String cardName; // Declares new string variable for the name of a royal card
         switch(card){
           case 0: // Case for when remainder is 0
              cardName ="King"; // Store string King in this case
              System.out.println("The " + cardName + " of " + suit); //Print out the card suit and name 
             break; // Exit switch loop
           case 1: // Case for when remainder is 1
                      // The case numbers do not necessarily correspond to any specific card, but they are the numbers that do not appear on cards
              cardName ="Queen"; // Store string Queen in this case
              System.out.println("The " + cardName + " of " + suit); //Print out the card suit and name
             break; // Exit switch loop
           case 11: // Case for when remainder is 11
              cardName ="Jack"; // Store string Jack in this case
              System.out.println("The " + cardName + " of " + suit); //Print out the card suit and name
             break; // Exit switch loop
           case 12: // Case for when remainder is 12
              cardName ="Ace"; // Store string Ace in this case
              System.out.println("The " + cardName + " of " + suit); //Print out the card suit and name
             break; // Exit switch loop
           default: // If no case is true, run default 
              System.out.println("The " + card + " of " + suit); //Print out the card suit and name using remainder value as name
             break; // Exit switch loop
         }
        } // End of main method
} // End of class